import os
import shutil


def ajouter_page_aoc_jour(numero_jour):
    ### Création du fichier dayXX.md

    #Check if we execute from the right folder
    if not (os.path.isfile('docs\day1.md') and os.path.isfile('mkdocs.yml')):
        print("On n'est pas dans le bon dossier pour executer la fonction d'ajout !!!")
        return 0
    else:
        print("Début de l'ajout")


    #Check if the file already exist:
    if os.path.isfile('docs\day'+numero_jour+'.md'):
        print('Le fichier docs\day'+numero_jour+'.md existe déjà ! Fin du script')
        return 0
    else:
        print("ajout en cours")

    # Check if pyhton file already exist and create it:
    if len(numero_jour) == 1:
        chemin_fichier_python = 'docs\scripts\\0'+numero_jour+'.py'
    elif len(numero_jour) == 2:
        chemin_fichier_python = 'docs\scripts\\'+numero_jour+'.py'

    if os.path.isfile(chemin_fichier_python):
        print("Le fichier python existe déjà")
    else:
        print("Création du fichier python")
        shutil.copyfile('docs\scripts\\template.py',chemin_fichier_python)
    

    # Read in the file
    with open('docs\dayX_template.md', 'r', encoding='utf-8') as file :
        filedata = file.read()

        # Replace the target string
        if len(numero_jour) == 1:
            new_filedata = filedata.format(numero_jour,numero_jour,numero_jour,'0'+numero_jour)
        elif len(numero_jour) == 2:
            new_filedata = filedata.format(numero_jour,numero_jour,numero_jour,numero_jour)


    # Write the file out again
    with open('docs\day'+numero_jour+'.md', 'w', encoding='utf-8') as file:
        file.write(new_filedata)

    ####  Ajout dans la nav du yml
    with open('mkdocs.yml', 'r', encoding='utf-8') as file:
        lines = file.readlines()
        index_jour_line = lines.index('    - Jour 01: day1.md\n')

        index_to_insert = index_jour_line+1
        tmp = lines[index_to_insert]
        while '- Jour' in tmp:
            index_to_insert+=1
            tmp = lines[index_to_insert]


        if len(numero_jour) == 1:
            lines.insert(index_to_insert,'    - Jour 0'+numero_jour+': day'+numero_jour+'.md\n')
        elif len(numero_jour) == 2:
            lines.insert(index_to_insert,'    - Jour '+numero_jour+': day'+numero_jour+'.md\n')

    with open('mkdocs.yml','w', encoding='utf-8') as file:
        file.writelines(lines)

ajouter_page_aoc_jour('4')


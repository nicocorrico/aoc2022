# aoc_day 1

#parse input
input = [line.strip() for line in open("01input.txt")]

def calories_transporte_par_chaque_lutin(data):
    "Retourne la liste du total transporté par chaque lutin"
    calories_par_lutin = []
    total_tmp = 0
    for elt in data:
        if elt != '':
            total_tmp += int(elt)
        else:
            calories_par_lutin.append(total_tmp)
            total_tmp = 0
    return calories_par_lutin

def part1(data):
    """Solve part 1"""
    return(max(calories_transporte_par_chaque_lutin(data)))

def part2(data):
    """Solve part 2"""
    calories_par_lutin = calories_transporte_par_chaque_lutin(data)
    result = 0
    for _ in range(3):
        result = result + calories_par_lutin.pop(calories_par_lutin.index(max(calories_par_lutin)))
    return result


def solve(data):
    """Solve the puzzle for the given input"""
    solution1 = part1(data)
    solution2 = part2(data)
    return solution1, solution2

print("\n".join(str(solution) for solution in solve(input)))
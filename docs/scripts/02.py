# aoc_jour2.py

#traitement de l'entrée
input = [line.strip() for line in open("02input.txt")]
#input_test=[]

def resultat_match(adversaire, joueur):
    if adversaire == 'A':
        if joueur == 'X':
            return 3
        elif joueur == 'Y':
            return 6
        else:
            return 0
    elif adversaire == 'B':
        if joueur == 'X':
            return 0
        elif joueur == 'Y':
            return 3
        else:
            return 6
    else:
        if joueur == 'X':
            return 6
        elif joueur == 'Y':
            return 0
        else:
            return 3

def score(match):
    score_signe = 0
    if match[1]=='X':
        score_signe = 1
    elif match[1]=='Y':
        score_signe = 2
    else:
        score_signe = 3
    return score_signe + resultat_match(match[0],match[1])

def part1(data):
    """Résout la partie 1"""
    #second traitement de l'entrée:
    matchs= [line.split() for line in data]
    score_total = 0
    for m in matchs:
        score_total += score(m)
    return score_total

def part2(data):
    """Résout la partie 2"""
    #second traitement de l'entrée:
    matchs= [line.split() for line in data]
    score_total = 0
    for m in matchs:
        if m[1]=='X':
            if m[0]=='A':
                score_total+=3
            elif m[0]=='B':
                score_total+=1
            else:
                score_total+=2
        elif m[1]=='Y':
            score_total+=3
            if m[0]=='A':
                score_total+=1
            elif m[0]=='B':
                score_total+=2
            else:
                score_total+=3
        else:
            score_total+=6
            if m[0]=='A':
                score_total+=2
            elif m[0]=='B':
                score_total+=3
            else:
                score_total+=1
    return score_total

def resout(data):
    """résout le problème"""
    solution1 = part1(data)
    solution2 = part2(data)
    return solution1, solution2

print("\n".join(str(solution) for solution in resout(input)))
# aoc_template.py

#traitement de l'entrée
input = [line.strip() for line in open("04input.txt")]
input_test=[]

def contient_entierement(zone1,zone2):
    """
    Renvoit True si la zone1 est entierement inclue dans zone2, False sinon
        Parametre:
            zone1 (str): de la forme 'a-b'
            Zone2 (str): de la forme 'a-b'
    """
    zone1 = [int(x) for x in zone1.split('-')]
    zone2 = [int(x) for x in zone2.split('-')]
    if zone1[0]>= zone2[0] and zone1[1]<=zone2[1]:
        return True
    else:
        return False

def recouvrement_partiel(zone1, zone2):
    """
    Renvoit True si la zone1 est partielement inclue dans zone2, False sinon
        Parametre:
            zone1 (str): de la forme 'a-b'
            Zone2 (str): de la forme 'a-b'
    """
    zone1 = [int(x) for x in zone1.split('-')]
    zone2 = [int(x) for x in zone2.split('-')]
    if zone1[1]< zone2[0] or zone1[0]>zone2[0]:
        return False
    else:
        return True

def part1(data):
    """Résout la partie 1"""
    #second traitement de l'entrée
    data =[(x.split(',')) for x in data]
    nombre_recouvrement = 0
    for paire in data:
        if contient_entierement(paire[0],paire[1]) or contient_entierement(paire[1],paire[0]):
            nombre_recouvrement+=1
    return nombre_recouvrement

def part2(data):
    """Résout la partie 2"""
    #second traitement de l'entrée
    data =[(x.split(',')) for x in data]
    nombre_recouvrement_partiel = 0
    for paire in data:
        if recouvrement_partiel(paire[0],paire[1]) or recouvrement_partiel(paire[1],paire[0]):
            nombre_recouvrement_partiel+=1
    return nombre_recouvrement_partiel

def resout(data):
    """résout le problème"""
    solution1 = part1(data)
    solution2 = part2(data)
    return solution1, solution2

print("\n".join(str(solution) for solution in resout(input)))
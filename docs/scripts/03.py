# aoc_template.py

#traitement de l'entrée
input = [line.strip() for line in open("03input.txt")]
input_test=[]

def element_commun(chaine1, chaine2):
    for e in chaine1:
        if e in chaine2:
            return e

def calculer_priorite(lettre):
    ref = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNUPQRSTUVWXYZ"
    return ref.index(lettre)+1

def part1(data):
    """Résout la partie 1"""
    somme = 0
    for sac in data:
        somme += calculer_priorite(element_commun(sac[0:int(len(sac)/2)],sac[int(len(sac)/2):]))
    return somme

def element_commun_groupe(chaine1, chaine2, chaine3):
    for e in chaine1:
        if e in chaine2:
            if e in chaine3:
                return e

def part2(data):
    """Résout la partie 2"""
    somme = 0
    for i in range(int(len(data)/3)):
        somme += calculer_priorite(element_commun_groupe(data[int(i*3)],data[int(i*3)+1],data[int(i*3)+2]))
    return somme

def resout(data):
    """résout le problème"""
    solution1 = part1(data)
    solution2 = part2(data)
    return solution1, solution2

print("\n".join(str(solution) for solution in resout(input)))
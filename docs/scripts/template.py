# aoc_template.py

#traitement de l'entrée
input = [line.strip() for line in open("input.txt")]
input_test=[]

def part1(data):
    """Résout la partie 1"""

def part2(data):
    """Résout la partie 2"""

def resout(data):
    """résout le problème"""
    solution1 = part1(data)
    solution2 = part2(data)
    return solution1, solution2

print("\n".join(str(solution) for solution in resout(input)))
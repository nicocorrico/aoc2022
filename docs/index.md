# Bienvenue sur ce site de test

Le but du site est de présenter mes solutions en Python aux défis Advent of Code 2022.
Le but de ce site est aussi pour moi l'occasion de tester la mise en ligne de documentation au format mkdocs avec le thème Material for mkdocs.
Je m'aide des sites <https://squidfunk.github.io/mkdocs-material/> et <https://ens-fr.gitlab.io/mkdocs/>


